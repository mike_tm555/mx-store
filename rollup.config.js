import resolve from 'rollup-plugin-node-resolve';
import pkg from './package.json';
import typescript from 'rollup-plugin-typescript';
import commonjs from 'rollup-plugin-commonjs';
import globalsPlugin from 'rollup-plugin-node-globals';
import babel from 'rollup-plugin-babel';
import replace from 'rollup-plugin-replace';

const globals = {
    react: 'React',
    'react-dom': 'ReactDOM',
};

export default {
    input: 'src/index.js',
    output: [
        {
            file: pkg.main,
            format: 'umd',
            name: 'mx-store',
            globals,
            sourcemap: true,
        },
        {
            file: pkg.module,
            format: 'esm',
            globals,
        },
    ],
    external: [...Object.keys(pkg.peerDependencies)],
    plugins: [
        resolve({
            browser: true,
        }),
        babel({
            exclude: 'node_modules/**',
            extensions: ['.js', '.jsx', '.ts', '.tsx'],
        }),
        typescript({
            exclude: ['**/__tests__/**', 'node_modules/**'],
        }),
        commonjs({
            include: 'node_modules/**',
            namedExports: {
                'node_modules/react-is/index.js': ['isValidElementType'],
            },
        }),
        replace({
            'process.env.NODE_ENV': JSON.stringify('production'),
        }),
        globalsPlugin(),
    ],
};
