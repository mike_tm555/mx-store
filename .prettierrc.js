module.exports = {
    printWidth: 150,
    singleQuote: true,
    tabWidth: 4,
};
