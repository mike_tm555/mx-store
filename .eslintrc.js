module.exports = {
    extends: ['eslint:recommended', 'prettier'],
    env: {
        browser: true,
        node: true,
        es6: true,
        'jest/globals': true,
    },
    plugins: ['jest', 'react', 'react-hooks'],
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly',
    },
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
        ecmaVersion: 2018,
        sourceType: 'module',
    },
    rules: {
        'no-unused-vars': ['error', { args: 'none' }],
    },
};
