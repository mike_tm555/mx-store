import React from 'react';
import Store from '../../store/storage';
import generateId from '../../heelpers/geenerate-id';

export default function (Component) {
    return function (connectProps) {
        return class extends React.Component {
            constructor(props) {
                super(props);
                this.subscriberKey = generateId();
                this.state = {
                    allProps: false,
                };
            }

            componentDidMount() {
                this.updateProps();
                Store.subscribe(this.subscriberKey, () => {
                    this.updateProps();
                });
            }

            componentWillUnmount() {
                Store.unsubscribe(this.subscriberKey);
            }

            updateProps() {
                const currentProps = {};
                const currentPromises = [];
                let connectedProps = connectProps;

                if (typeof connectProps === 'function') {
                    connectedProps = connectProps(Store.storage);
                }

                Object.keys(connectedProps).forEach((key) => {
                    let currentProp = connectedProps[key];

                    if (currentProp instanceof Promise) {
                        currentPromises.push(currentProp);

                        currentProps[key] = Object.create(currentProp, {
                            loading: {
                                get: function () {
                                    return currentProp.isResolved !== true;
                                },
                            },
                        });
                        currentProp.then((data) => {
                            currentProps[key] = data;
                        });
                    } else {
                        currentProps[key] = currentProp;
                    }
                });

                if (currentPromises.length) {
                    Promise.all(currentPromises).then((data) => {
                        if (!data.error) {
                            this.setState({
                                allProps: currentProps,
                            });
                        }
                    });
                }

                this.setState({
                    allProps: currentProps,
                });
            }

            render() {
                const currentProps = this.state.allProps;

                if (currentProps) {
                    Object.keys(this.props).forEach((key) => {
                        currentProps[key] = this.props[key];
                    });

                    return <Component {...currentProps} />;
                }

                return null;
            }
        };
    };
}
