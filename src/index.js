import Store, { STORE_MIXINS } from './store/storage';
import mxReactConnect from './plugins/react/connect';
import CreateStore from './store/create';

export { Store as mxStore, mxReactConnect, CreateStore as mxCreate, STORE_MIXINS as mxMixins };
