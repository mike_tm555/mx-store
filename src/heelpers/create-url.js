export default function createURL(params) {
    let url = params.host;

    if (params.path) {
        url += params.path;
    }

    if (params.id) {
        url += `/${params.id}`;
    }

    return url;
}
