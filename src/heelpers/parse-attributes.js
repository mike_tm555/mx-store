export default function parseAttributes(item, attributes, type) {
    if (!attributes) {
        item.type = type;
        return item;
    }

    let parsedItem = {};

    Object.keys(attributes).forEach((attrKey) => {
        const matchingTypes = attributes[attrKey].split(' || ');

        let typeIsCorrect = false;

        matchingTypes.forEach((type) => {
            if (typeof item[attrKey] === type) {
                typeIsCorrect = true;
            }
        });

        if (typeIsCorrect) {
            parsedItem[attrKey] = item[attrKey];
        } else {
            throw new Error(`${attrKey} should be a type ${attributes[attrKey]}`);
        }
    });

    parsedItem.type = type;

    return parsedItem;
}
