export default function createQueryString(params) {
    let queryString = '';

    if (Object.keys(params)) {
        queryString = '?';

        const paramsMap = Object.keys(params).map((key) => {
            return `${key}=${params[key]}`;
        });

        queryString += paramsMap.join('&');
    }

    return queryString;
}
