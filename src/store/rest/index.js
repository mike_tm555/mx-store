import createQueryString from '../../heelpers/querry-string';
import generateId from '../../heelpers/geenerate-id';
import createURL from '../../heelpers/create-url';
import update from './update';

const CACHE = {};

export default class extends Array {
    constructor(props) {
        super();

        Object.defineProperties(this, {
            primaryKey: {
                value: 'id',
            },
            loading: {
                value: null,
                writable: true,
            },
        });

        Object.keys(props).forEach((key) => {
            Object.defineProperty(this, key, {
                value: props[key],
            });
        });
    }

    getData() {
        return this;
    }

    setData(data) {
        this.splice(0, this.length);
        data.forEach((item) => this.push(item));
    }

    find(options) {
        const posts = this.findAll(options);

        if (posts.length) {
            return posts[0];
        }

        return null;
    }

    findAll(options) {
        let posts = [];

        this.forEach((item) => {
            let allOptionsMatch = true;

            Object.keys(options).forEach((key) => {
                if (item[key] !== options[key]) {
                    allOptionsMatch = false;
                }
            });

            if (allOptionsMatch === true) {
                posts.push(item);
            }
        });

        return posts;
    }

    async fetch(id) {
        let fetchURL = createURL({
            host: this.host,
            path: this.path,
            [this.primaryKey]: id,
        });

        const cacheKey = `${this.name}-fetch-${id}`;

        if (CACHE[cacheKey]) {
            return CACHE[cacheKey];
        }

        this.loading = true;

        CACHE[cacheKey] = fetch(fetchURL)
            .then((response) => {
                return response.json();
            })
            .then((json) => {
                let data = null;

                if (typeof this.serialize === 'function') {
                    data = this.serialize(json);
                } else {
                    data = json.data;
                }

                update(this.name, [data], () => {
                    this.loading = false;
                });

                delete CACHE[cacheKey];

                return data;
            });

        return CACHE[cacheKey];
    }

    async fetchAll() {
        let fetchURL = createURL({
            host: this.host,
            path: this.path,
        });

        const cacheKey = `${this.name}-fetchAll`;

        if (CACHE[cacheKey]) {
            return CACHE[cacheKey];
        }

        this.loading = true;

        CACHE[cacheKey] = fetch(fetchURL)
            .then((response) => {
                return response.json();
            })
            .then((json) => {
                let data = null;

                if (typeof this.serialize === 'function') {
                    data = this.serialize(json);
                } else {
                    data = json.data;
                }

                update(this.name, data, () => {
                    this.loading = false;
                });

                delete CACHE[cacheKey];

                return data;
            });

        return CACHE[cacheKey];
    }

    async query(queryParams) {
        let queryUrl = createURL({
            host: this.host,
            path: this.path,
        });

        if (queryParams) {
            queryUrl += createQueryString(queryParams);
        }

        const cacheKey = `${this.name}-query-${queryUrl}`;

        if (CACHE[cacheKey]) {
            return CACHE[cacheKey];
        }

        this.loading = true;

        CACHE[cacheKey] = fetch(queryUrl)
            .then((response) => {
                return response.json();
            })
            .then((json) => {
                let data = null;

                if (typeof this.serialize === 'function') {
                    data = this.serialize(json);
                } else {
                    data = json.data;
                }

                update(this.name, data, () => {
                    this.loading = false;
                });

                delete CACHE[cacheKey];

                return data;
            });

        return CACHE[cacheKey];
    }

    async get(id) {
        const post = this.find({ [this.primaryKey]: id });

        if (!post) {
            return this.fetch(id);
        }

        return post;
    }

    async getAll() {
        if (this.loading === false) {
            return this;
        }

        return this.fetchAll();
    }

    async delete(data) {
        const newData = this.filter((item) => {
            return item[this.primaryKey] !== data[this.primaryKey];
        });

        update(this.name, newData, () => {
            this.loading = true;

            let fetchURL = createURL({
                host: this.host,
                path: `${this.path}/${data[this.primaryKey]}`,
            });

            fetch(fetchURL).then((response) => {
                this.loading = false;
            });
        });
    }

    async create(value) {
        const tempId = generateId();

        if (value[this.primaryKey] === undefined) {
            value[this.primaryKey] = tempId;
        }

        update(this.name, [value], () => {
            let fetchURL = createURL({
                host: this.host,
                path: this.path,
            });

            this.loading = true;

            fetch(fetchURL, {
                method: 'PUT',
                body: JSON.stringify(value),
            })
                .then((response) => {
                    return response.json();
                })
                .then((json) => {
                    let data = null;

                    if (typeof this.serialize === 'function') {
                        data = this.serialize(json);
                    } else {
                        data = json.data;
                    }

                    const newData = this.map((item) => {
                        if (item[this.primaryKey] === tempId) {
                            return data;
                        }

                        return item;
                    });

                    update(this.name, [newData], () => {
                        this.loading = false;
                    });
                });
        });

        return this.find(value[this.primaryKey]);
    }
}
