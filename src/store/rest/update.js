import Store from '../storage';
import parseAttributes from '../../heelpers/parse-attributes';

function setRelationShips(data) {
    const relationships = Store.getRelationships(data.type);

    relationships.forEach((relationship) => {
        switch (relationship.type) {
            case 'many-to-one':
            case 'one-to-one': {
                if (data[relationship.to]) {
                    update(relationship.to, [data[relationship.to]], () => {
                        data[relationship.to] = Store.get(data[relationship.to].type, data[relationship.to].id);
                    });
                }
                break;
            }
            case 'one-to-many':
            case 'many-to-many': {
                if (data[relationship.to]) {
                    update(relationship.to, data[relationship.to], () => {
                        data[relationship.to] = data[relationship.to].map((item) => {
                            return Store.get(item.type, item.id);
                        });
                    });
                }
                break;
            }
        }
    });

    return data;
}

export default function update(type, data, callback) {
    const primaryKey = Store.getPrimaryKey(type);
    let newData = [];
    data.forEach((dataItem) => {
        let dataItemExist = false;

        Store.get(type).forEach((storageItem) => {
            if (storageItem[primaryKey] === dataItem[primaryKey]) {
                dataItemExist = true;
                newData.push(setRelationShips(parseAttributes(dataItem, Store.getAttributes(type), type)));
            } else if (!newData.includes(storageItem)) {
                newData.push(storageItem);
            }
        });

        if (dataItemExist === false) {
            newData = newData.concat(setRelationShips(parseAttributes(dataItem, Store.getAttributes(type), type)));
        }
    });

    if (typeof callback === 'function') {
        callback();
    }

    Store.set(type, newData);
}
