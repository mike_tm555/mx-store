import Store from '../storage';
import parseAttributes from '../../heelpers/parse-attributes';

export default function update(type, data, callback) {
    const newData = parseAttributes(data, Store.getAttributes(type), type);

    if (typeof callback === 'function') {
        callback();
    }

    Store.set(type, newData);
}
