import update from './update';

export default class {
    constructor(props) {
        Object.defineProperties(this, {
            loading: {
                value: null,
                writable: true,
            },
        });

        Object.keys(props).forEach((key) => {
            Object.defineProperty(this, key, {
                value: props[key],
            });
        });
    }

    getData() {
        return this.json();
    }

    setData(data) {
        window.localStorage.setItem(this.name, JSON.stringify(data));
    }

    string() {
        return window.localStorage.getItem(this.name);
    }

    json() {
        const savedItem = window.localStorage.getItem(this.name);
        return savedItem ? JSON.parse(savedItem) : {};
    }

    save(data) {
        const newData = this.json();

        Object.keys(data).forEach((key) => {
            newData[key] = data[key];
        });

        update(this.name, data, () => {
            this.loading = false;
        });
    }

    delete(key) {
        const newData = this.json();

        delete newData[key];

        this.loading = true;

        update(this.name, newData, () => {
            this.loading = false;
        });
    }

    clear() {
        console.log('should clear all data');
    }
}
