import Rest from './rest/index';
import Local from './local/index';
import Session from './session/index';

export const STORE_MIXINS = {
    Rest,
    Local,
    Session,
};

export class Storage {
    constructor(options = {}) {
        this.type = options.type || 'Rest';
        this.storage = {};
        this.observers = {};
    }

    set(type, data) {
        this.storage[type].setData(data);

        Object.values(this.observers).forEach((observer) => {
            observer();
        });
    }

    get(type, id = null) {
        if (id === null) {
            return this.storage[type].getData();
        }

        return this.storage[type].find({ id });
    }

    getRelationships(type) {
        return this.storage[type].relationships || [];
    }

    getAttributes(type) {
        return this.storage[type].attributes;
    }

    getPrimaryKey(type) {
        return this.storage[type].primaryKey;
    }

    subscribe(key, observer) {
        this.observers[key] = observer;
    }

    unsubscribe(key) {
        delete this.observers[key];
    }

    register(options = {}) {
        const modelName = options.name;

        if (this.storage[modelName] === undefined) {
            this.storage[modelName] = new STORE_MIXINS[options.type || this.type](options);
        }

        return this.storage[modelName];
    }
}

let instance = null;

function Store(options) {
    if (instance === null) {
        instance = new Storage(options);
    }

    return instance;
}

export default Store();
