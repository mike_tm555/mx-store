import Store from './storage';

export default function CreateStore(models) {
    return models.map((model) => {
        return Store.register(model);
    });
}
