module.exports = {
    '*.{js,jsx}': ['yarn lint -- --fix'],
    '*.{js,jsx,ts,tsx,css,md,yml,json}': ['prettier --write'],
};
