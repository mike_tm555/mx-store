const { defaults } = require('jest-config');

module.exports = {
    roots: ['<rootDir>/src'],
    preset: 'ts-jest',
    testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$',
    moduleFileExtensions: [...defaults.moduleFileExtensions, 'ts', 'tsx'],
    testEnvironment: 'jsdom',
    transformIgnorePatterns: ['/node_modules/'],
};
